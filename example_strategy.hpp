#ifndef EXAMPLE_STRATEGY_HPP
#define EXAMPLE_STRATEGY_HPP
#include <iostream>

template< int Z >
struct strategy
{
  using return_type = void;

  static return_type true_statement()
  {
    std::cout << "true happend\n";
  }

  static return_type false_statement()
  {
    std::cout << "false happend\n";
  }

  static return_type loop_statement()
  {
    std::cout << "loop statement\n";
  }
};
#endif
