#include <iostream>
#include "if_statement.hpp"
#include "loop_statement.hpp"
#include "example_strategy.hpp"

int odd_factorial(int n);

template< int N, template< int Z > typename Strategy >
using meta_odd_factorial = meta_loop< N, Strategy >;

template< int Z >
struct odd_factorial_strategy;

int main()
{
  meta_if< true, strategy< 0 > >::statement();
  meta_if< false, strategy< 0 > >::statement();
  meta_loop< 3, strategy >::statement();
  std::cout << "Trying implement odd-factorial as metaprogramm...\n";
  meta_odd_factorial< 4, odd_factorial_strategy >::statement();
}

int odd_factorial(int n)
{
  auto ret = 1;
  for(auto i = n; n; --i)
  {
    if(!(i % 2))
    {
      ret *= i;
    }
  }
  return ret;
}

template< int Z >
struct odd_factorial_strategy
{
  static void loop_statement()
  {
    meta_if< !(Z % 2), odd_factorial_strategy< 0 > >::statement();
  }

  static void true_statement()
  {
    std::cout << "Number is odd. Multiply by number...\n";
  }

  static void false_statement()
  {
    std::cout << "Number is even\n";
  }
};
