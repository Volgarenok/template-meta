#ifndef LOOP_STATEMENT_HPP
#define LOOP_STATEMENT_HPP
template< int I, template < int Z > typename Strategy >
class meta_loop
{
  public:
    static void statement()
    {
      Strategy< I >::loop_statement();
      meta_loop< more ? (I - 1) : 0, Strategy >::statement();
    }
  private:
    enum
    {
      more = (I - 1)
    };
};

template< template< int Z > typename Strategy >
class meta_loop< 0, Strategy >
{
  public:
    static void statement()
    {
      Strategy< 0 >::loop_statement();
    }
};
#endif
