#ifndef IF_STATEMENT_HPP
#define IF_STATEMENT_HPP
template< bool Condition, typename Strategy >
class meta_if {};

template< typename Strategy >
class meta_if< true, Strategy >
{
  public:
    static void statement()
    {
      Strategy::true_statement();
    }
};

template< typename Strategy >
class meta_if< false, Strategy >
{
  public:
    static void statement()
    {
      Strategy::false_statement();
    }
};
#endif
