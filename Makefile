.PHONY: clean
.PHONY: help

SRC_LIST = $(wildcard *.cpp)

help:
	@echo "make ..."
	@echo "     help [default] - show this help"
	@echo "     meta           - demo for using if-statements and loop-statements"
	@echo "                      Generalizing this statements is non trivial task"

meta: $(SRC_LIST)
	$(CXX) -o $@ $^
